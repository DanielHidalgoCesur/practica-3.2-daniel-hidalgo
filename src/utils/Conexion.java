package utils;

import controllers.GrupoControler;
import models.Grupo;

import java.sql.*;

public class Conexion {

    private String cadena_conexion = "jdbc:mysql://localhost:3306/GruposRock?user=root";
    private String usuario = "root";
    private String contra = "1234";

    public Conexion(String cadena_conexion, String usuario, String contra) {
        this.cadena_conexion = cadena_conexion;
        this.usuario = usuario;
        this.contra = contra;
    }

    public Connection getConnection(){
        try {
            Connection conn = DriverManager.getConnection(this.cadena_conexion,this.usuario,this.contra);
            return conn;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
    public void closeConnection(Connection conn){
        try {
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
