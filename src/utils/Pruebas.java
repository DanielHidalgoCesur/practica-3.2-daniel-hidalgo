package utils;

import controllers.DiscografiaControler;
import controllers.GrupoControler;
import models.Discografia;
import models.Grupo;
import models.Musica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Pruebas {
    public static void main(String[] args) {
        // characterEncoding=UTF-8
        //String cadena_conexion = "jdbc:mysql://localhost:3306/hr?useUnicode=true&serverTimezone=UTC";
        String cadena_conexion = "jdbc:mysql://127.0.0.1:3306/GruposRock?user=root";
        String usuario = "root";
        String contrasena = "1234";
        System.out.println("Tabla Grupo:");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from grupo");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                //int idGrupo, String nombre, String cancion, String origen, String vocalista
                int id = rs.getInt("idGrupos");
                String nombre = rs.getString("nombre");
                String cancion = rs.getString("cancion");
                String origen = rs.getString("origen");
                String vocalista = rs.getString("vocalista");
                Grupo g1 = new Grupo(id, nombre, cancion, origen, vocalista);
                System.out.println(g1);


            }
            rs.close();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("Tabla Musica:");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from musica");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                //int idMusica, int idGrupo, String titulo, String soporte, String genero
                int id = rs.getInt("idMusica");
                int idGrupo = rs.getInt("idGrupo");
                String titulo = rs.getString("titulo");
                String soporte = rs.getString("soporte");
                String genero = rs.getString("genero");
                Musica m1 = new Musica(id, idGrupo, titulo, soporte, genero);
                System.out.println(m1);
            }
            rs.close();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("Tabla Discografia:");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from discografia");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                //String tituloDisco, String soporte, String grupo, int estante, int idMusica
                String titulo = rs.getString("tituloDisco");
                String soporte = rs.getString("soporte");
                String grupo = rs.getString("grupo");
                int estante = rs.getInt("estante");
                int id = rs.getInt("idMusica");
                Discografia d1 = new Discografia(titulo, soporte, grupo, estante, id);
                System.out.println(d1);
            }
            rs.close();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
