package controllers;

import models.Discografia;
import models.DiscografiaDOD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DiscografiaControler implements DiscografiaDOD {
    private final String SELECT_ALL = "select * from discografia";
    private Connection con;
    public DiscografiaControler(Connection con) {
        this.con = con;
    }


    @Override
    public List<Discografia> selectAllDiscografia() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Discografia> lista = new ArrayList<Discografia>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Discografia g = new Discografia(rs.getString("tituloDisco"), rs.getString("soporte"), rs.getString("grupo"), rs.getString("estante"), rs.getString("idMusica"));
                lista.add(g);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    @Override
    public Discografia insertDiscografia(String tituloDisco, String soporte, String grupo, String estante, String idMusica) {
        return null;
    }


    public Discografia insertDiscografia(String tituloDisco, String soporte, String grupo, int estante, int idMusica) {
        String insert = "insert into discografia (tituloDisco, soporte, grupo, estante,  idMusica) values(?,?,?,?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setString(1,tituloDisco);
            stm.setString(2, soporte);
            stm.setString(3, grupo);
            stm.setInt(4, estante);
            stm.setInt(5, idMusica);

            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Discorgrafia ha sido insertado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteDiscografia(String tituloDisco) {
        String sql = "DELETE FROM discografia WHERE tituloDisco=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, tituloDisco);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("La discografia " + tituloDisco + " ha sido eliminada");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    @Override
    public Discografia updateDiscografia(String soporte) {
        return null;
    }

    public Discografia updateDiscografia(String soporte, String titulo) {
        String sql = "UPDATE discografia SET soporte=? where titulo=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, soporte);
            statement.setString(2, titulo);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado discografia");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectDiscografia(String soporte) {
        return null;
    }
}
