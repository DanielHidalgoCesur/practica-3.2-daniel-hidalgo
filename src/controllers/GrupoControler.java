package controllers;

import models.Discografia;
import models.Grupo;
import models.GrupoDOD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GrupoControler implements GrupoDOD {
    private final String SELECT_ALL = "select * from grupo";
    private Connection con;

    public GrupoControler(Connection con) {
        this.con = con;
    }

    @Override
    public List<Grupo> selectAllGrupo() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Grupo> lista = new ArrayList<Grupo>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Grupo g = new Grupo(rs.getString("idGrupo"), rs.getString("nombre"), rs.getString("cancion"), rs.getString("origen"), rs.getString("vocalista"));
                lista.add(g);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    @Override
    public Grupo insertGrupo(int idGrupo, String nombre, String cancion, String origen, String vocalista) {
        String insert = "insert into grupo (idGrupo, nombre, cancion, origen,  vocalista) values(?,?,?,?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setInt(1,idGrupo);
            stm.setString(2, nombre);
            stm.setString(3, cancion);
            stm.setString(4, origen);
            stm.setString(5, vocalista);

            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Grupo ha sido insertado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteGrupo(int idGrupo) {
        String sql = "DELETE FROM grupo WHERE idGrupo=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, idGrupo);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("EL grupo " + idGrupo + " ha sido eliminado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    @Override
    public Grupo updateGrupo(String nombre) {
        return null;
    }

    public Grupo updateGrupo(String nombre, int idGrupo) {
        String sql = "UPDATE grupo SET nombre=? where idGrupo=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre);
            statement.setInt(2, idGrupo);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado grupo");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectGrupo(String nombre) {
        return null;
    }
}
