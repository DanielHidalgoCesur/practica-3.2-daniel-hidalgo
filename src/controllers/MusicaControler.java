package controllers;

import models.Grupo;
import models.Musica;
import models.MusicaDOD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MusicaControler implements MusicaDOD {
    private final String SELECT_ALL = "select * from musica";
    private Connection con;

    public MusicaControler(Connection con) {
        this.con = con;
    }

    @Override
    public List<Musica> selectAllMusica() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Musica> lista = new ArrayList<Musica>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            while (rs.next()) {
                Musica m = new Musica(rs.getString("idMusica"), rs.getString("idGrupo"), rs.getString("titulo"), rs.getString("soporte"), rs.getString("genero"));
                lista.add(m);
            }
            //rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    @Override
    public Musica insertMusica(int idMusica, int idGrupo, String titulo, String soporte, String genero) {
        String insert = "insert into grupo (idMusica, idGrupo, titulo, soporte,  genero) values(?,?,?,?,?)";

        try {
            PreparedStatement stm = con.prepareStatement(insert);
            stm.setInt(1,idMusica);
            stm.setInt(2, idGrupo);
            stm.setString(3, titulo);
            stm.setString(4, soporte);
            stm.setString(5, genero);

            int rowsInserted = stm.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Musica ha sido insertado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteMusica(int idMusica) {
        String sql = "DELETE FROM musica WHERE idMusica=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, idMusica);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("La musica " + idMusica + " ha sido eliminada");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    @Override
    public Musica updateMusica(String titulo) {
        return null;
    }

    public Musica updateMusica(String titulo, int idMusica) {
        String sql = "UPDATE grupo SET titulo=? where idMusica=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, titulo);
            statement.setInt(2, idMusica);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado grupo");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectMusica(String titulo) {
        return null;
    }
}
