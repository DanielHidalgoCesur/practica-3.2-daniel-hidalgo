package test;

import controllers.DiscografiaControler;
import controllers.GrupoControler;
import controllers.MusicaControler;
import models.Discografia;
import models.Grupo;
import models.Musica;
import utils.Conexion;

import java.sql.Connection;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Conexion test = new Conexion("jdbc:mysql://localhost:3306/GruposRock?user=root", "root", "1234");
        Connection conn = test.getConnection();

        DiscografiaControler dc = new DiscografiaControler(conn);

        List<Discografia> lista = dc.selectAllDiscografia();
        for (Discografia c : lista) {
            System.out.println(c);
        }
        Discografia discografia = dc.insertDiscografia("Nevermind","cd","Nirvana","34","1");
        Discografia discografia1 = dc.updateDiscografia("cd","Nevermind");
        boolean discografia2 = dc.deleteDiscografia("Nevermind");

        GrupoControler gc = new GrupoControler(conn);
        List<Grupo> listag = gc.selectAllGrupo();
        for (Grupo c : listag) {
            System.out.println(c);
        }
        Grupo grupo = gc.insertGrupo(2,"yo","yomismo","micasa","eren");
        Grupo grupo1 = gc.updateGrupo("manolete", 2);
        boolean grupo2 = gc.deleteGrupo(4);

        MusicaControler mc = new MusicaControler(conn);
        List<Musica> listam = mc.selectAllMusica();
        for (Musica c : listam) {
            System.out.println(c);
        }
        Musica musica = mc.insertMusica(2,3,"hola","adios","hola de nuevo");
        Musica musica1 = mc.updateMusica("ciao", 3);
        boolean musica2 = mc.deleteMusica(567);
    }
}
