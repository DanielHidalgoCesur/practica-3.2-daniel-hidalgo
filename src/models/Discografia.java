package models;

public class Discografia {
    private String tituloDisco;
    private String soporte;
    private String grupo;
    private int estante;
    private int idMusica;

    public Discografia(String tituloDisco, String soporte, String grupo, int estante, int idMusica) {
        this.tituloDisco = tituloDisco;
        this.soporte = soporte;
        this.grupo = grupo;
        this.estante = estante;
        this.idMusica = idMusica;
    }

    public Discografia(String tituloDisco, String soporte, String grupo, String estante, String idMusica) {
    }

    @Override
    public String toString() {
        return "Discografia{" +
                "tituloDisco='" + tituloDisco + '\'' +
                ", soporte='" + soporte + '\'' +
                ", grupo='" + grupo + '\'' +
                ", estante=" + estante +
                ", idMusica=" + idMusica +
                '}';
    }

    public String getTituloDisco() {
        return tituloDisco;
    }

    public void setTituloDisco(String tituloDisco) {
        this.tituloDisco = tituloDisco;
    }

    public String getSoporte() {
        return soporte;
    }

    public void setSoporte(String soporte) {
        this.soporte = soporte;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public int getEstante() {
        return estante;
    }

    public void setEstante(int estante) {
        this.estante = estante;
    }

    public int getIdMusica() {
        return idMusica;
    }

    public void setIdMusica(int idMusica) {
        this.idMusica = idMusica;
    }
}
