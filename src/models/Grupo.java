package models;

public class Grupo {
    private int idGrupo;
    private String nombre;
    private String cancion;
    private String origen;
    private String vocalista;

    public Grupo(int idGrupo, String nombre, String cancion, String origen, String vocalista) {
        this.idGrupo = idGrupo;
        this.nombre = nombre;
        this.cancion = cancion;
        this.origen = origen;
        this.vocalista = vocalista;
    }

    public Grupo(String idGrupo, String nombre, String cancion, String origen, String vocalista) {
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public String toString() {
        return "Grupo{" +
                "idGrupo=" + idGrupo +
                ", nombre='" + nombre + '\'' +
                ", cancion='" + cancion + '\'' +
                ", origen='" + origen + '\'' +
                ", vocalista='" + vocalista + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCancion() {
        return cancion;
    }

    public void setCancion(String cancion) {
        this.cancion = cancion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getVocalista() {
        return vocalista;
    }

    public void setVocalista(String vocalista) {
        this.vocalista = vocalista;
    }
}
