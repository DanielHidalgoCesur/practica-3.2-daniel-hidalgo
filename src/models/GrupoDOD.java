package models;

import java.sql.ResultSet;
import java.util.List;

public interface GrupoDOD {
    public List<Grupo> selectAllGrupo();
    public Grupo insertGrupo(int idGrupo, String nombre, String cancion, String origen, String vocalista);
    public boolean deleteGrupo(int idGrupo);
    public Grupo updateGrupo(String nombre);
    // extra
    public ResultSet selectGrupo(String nombre);
}
