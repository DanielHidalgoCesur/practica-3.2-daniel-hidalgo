package models;

import java.sql.ResultSet;
import java.util.List;

public interface MusicaDOD {
    public List<Musica> selectAllMusica();
    public Musica insertMusica(int idMusica, int idGrupo, String titulo, String soporte, String genero);
    public boolean deleteMusica(int idMusica);
    public Musica updateMusica(String titulo);
    // extra
    public ResultSet selectMusica(String titulo);
}
