package models;

import java.sql.ResultSet;
import java.util.List;

public interface DiscografiaDOD {
    public List<Discografia> selectAllDiscografia();
    public Discografia insertDiscografia(String tituloDisco, String soporte, String grupo, String estante, String idMusica);
    public boolean deleteDiscografia(String tituloDisco);
    public Discografia updateDiscografia(String soporte);
    // extra
    public ResultSet selectDiscografia(String soporte);
}
