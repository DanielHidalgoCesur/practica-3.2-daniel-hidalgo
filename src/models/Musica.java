package models;

public class Musica {
    private int idMusica;
    private int idGrupo;
    private String titulo;
    private String soporte;
    private String genero;

    public Musica(int idMusica, int idGrupo, String titulo, String soporte, String genero) {
        this.idMusica = idMusica;
        this.idGrupo = idGrupo;
        this.titulo = titulo;
        this.soporte = soporte;
        this.genero = genero;
    }

    public Musica(String idMusica, String idGrupo, String titulo, String soporte, String genero) {
    }

    @Override
    public String toString() {
        return "Musica{" +
                "idMusica=" + idMusica +
                ", idGrupo=" + idGrupo +
                ", titulo='" + titulo + '\'' +
                ", soporte='" + soporte + '\'' +
                ", genero='" + genero + '\'' +
                '}';
    }

    public int getIdMusica() {
        return idMusica;
    }

    public void setIdMusica(int idMusica) {
        this.idMusica = idMusica;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSoporte() {
        return soporte;
    }

    public void setSoporte(String soporte) {
        this.soporte = soporte;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
